package it.marcoaceti;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

class Geocoding {
    private String filename;
    private boolean saved = false;
    private boolean parsed = false;
    private double latitude;
    private double longitude;

    public Geocoding(String address, String filename) {
        URL server;
        HttpsURLConnection service;
        BufferedReader input;
        BufferedWriter output;
        String line;
        int status;

        this.filename = filename;

        try {
            String prefix = "https://maps.googleapis.com/maps/api/geocode/xml?key=AIzaSyDSSOWTvCtX1EmtDMunUfNqRJSbxzI3Tsk&address=";
            String url = prefix + URLEncoder.encode(address, StandardCharsets.UTF_8); // costruzione dello URL di interrogazione del servizio
            server = new URL(url);
            service = (HttpsURLConnection) server.openConnection();
            service.setRequestProperty("Host", "maps.googleapis.com"); // impostazione header richiesta: host interrogato
            service.setRequestProperty("Accept", "application/xml"); // impostazione header richiesta: formato risposta (XML)
            service.setRequestProperty("Accept-Charset", "UTF-8"); // impostazione header richiesta: codifica risposta (UTF-8)
            service.setRequestMethod("GET"); // impostazione metodo di richiesta GET
            service.setDoInput(true); // attivazione ricezione
            service.connect(); // connessione al servizio
            status = service.getResponseCode(); // verifica stato risposta
            if (status != 200) {
                return; // errore
            }
            // apertura stream di ricezione da risorsa web
            input = new BufferedReader(new InputStreamReader(service.getInputStream(), StandardCharsets.UTF_8));
            // apertura stream per scrittura su file
            output = new BufferedWriter(new FileWriter(filename));
            // ciclo di lettura da web e scrittura su file
            while ((line = input.readLine()) != null) {
                output.write(line);
                output.newLine();
            }
            input.close();
            output.close();
            saved = true;
        }
        catch (IOException ignored) {}
    }

    public boolean isSaved() {
        return saved;
    }

    public boolean isParsed() {
        return parsed;
    }

    private void parseXML() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(filename);
            Element root = document.getDocumentElement();
            NodeList list = root.getElementsByTagName("status");
            if (list != null && list.getLength() > 0) {
                if (list.item(0).getFirstChild().getNodeValue().equalsIgnoreCase("OK")) {
                    list = root.getElementsByTagName("location");
                    if (list != null && list.getLength() > 0) {
                        Element loc = (Element)list.item(0);
                        NodeList lat = loc.getElementsByTagName("lat");
                        latitude = Double.parseDouble(lat.item(0).getFirstChild().getNodeValue());
                        NodeList lng = loc.getElementsByTagName("lng");
                        longitude = Double.parseDouble(lng.item(0).getFirstChild().getNodeValue());
                        parsed = true;
                    }
                }
            }
        }
        catch (IOException | ParserConfigurationException | SAXException e) {
            throw new GeocodingException();
        }
    }

    public double getLatitude() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        if (!parsed)
            parseXML();
        return latitude;
    }

    public double getLongitude() throws GeocodingException {
        if (!saved) {
            throw new GeocodingException();
        }
        if (!parsed)
            parseXML();
        return longitude;
    }

 /*
 public static void main(String[] args) {
  if (args.length < 2)
      System.err.println("Errore argomenti!");
  else {
    Geocoding geocoding = new Geocoding(args[0], args[1]);
    if (geocoding.isSaved())
        System.out.println("File XML salvato.");
    else
        System.err.println("Errore interrogazione servizio!");
  }
 }
 */


}
