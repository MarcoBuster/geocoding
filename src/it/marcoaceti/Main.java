package it.marcoaceti;

class Main {
    public static void main(String[] args) {
        Geocoding zanichelli = new Geocoding(
                "McDonald's, 11, Viale Alessandro Manzoni, " +
                        "Treviglio, Bergamo, Lombardy, 24047, Italy",
                "file.xml"
        );
        try {
            System.out.println("(" + zanichelli.getLatitude() + ";" + zanichelli.getLongitude() + ")");
        }
        catch (GeocodingException e) {
            System.err.println("Errore interrogazione servizio!");
        }
    }
}
